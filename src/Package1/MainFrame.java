package Package1;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame implements ActionListener {

    /** FIELDS */
    private JTextField jTextField1 = new JTextField();
    private JTextField jTextField2 = new JTextField();
    private JButton jButton = new JButton("CHECK");
    private String answer;
    private StringBuilder textForTextField = new StringBuilder();
    private String[] words = {"weapon","tree","three","congress","bicycle","run","freeman","egg",
    "implements","tee"};



    /** CONSTRUCTOR */
    public MainFrame(){

        JOptionPane.showMessageDialog(null,"Proposed a word, try to guess it");

        /** ADDS */
        add(jTextField1);
        add(jTextField2);
        add(jButton);

        /** COMPONENTS */

        jTextField1.setBounds(50,20,70,20);
        jTextField1.setBorder(new MatteBorder(1,1,1,1,new Color(0x000000)));
        jTextField1.setBackground(new Color(0x979891));
        jTextField1.setText(String.valueOf(setTextFieldx()));
        jTextField1.setEditable(false);
        jTextField2.setBounds(250,20,70,20);
        jTextField2.setBorder(null);
        jButton.setBounds(140,70,80,20);
        jButton.setBackground(new Color(0x64828C));
        jButton.setForeground(new Color(0x000005));

        /** ACTION LISTENERS */
        jButton.addActionListener(this);
        jTextField2.addActionListener(this);


        /** FRAME */
        setTitle("Guess the word");
        setLayout(null);
        getContentPane().setBackground(new Color(0x827D7A));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,150);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);

    }

    public StringBuilder setTextFieldx(){
        StringBuilder x = new StringBuilder();
        answer = words[(int) (Math.random()*10)];
        for (int i = 0; i < answer.length();i++){
            textForTextField.append("*");
            x.append("*");
        }
        return x;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            if (e.getSource() == jButton || e.getSource() == jTextField2){
                boolean t = false;
                for (int index = 0; index < answer.length();index++){
                    if (String.valueOf(jTextField2.getText()).equalsIgnoreCase(String.valueOf(answer.charAt(index)))){
                        t = true;
                    }
                }
                if (!t) {
                    JOptionPane.showMessageDialog(null, "Wrong letter, please try again");
                    jTextField2.setText("");
                }

                if (jTextField2.getText().equalsIgnoreCase(answer)){
                    jTextField1.setText(answer);
                    JOptionPane.showMessageDialog(null,"You have guessed the word");
                }
                else{
                    for (int i = 0; i < answer.length();i++){
                        if (String.valueOf(jTextField2.getText()).equalsIgnoreCase(String.valueOf(answer.charAt(i)))){

                            if (String.valueOf(jTextField2.getText()).equalsIgnoreCase(String.valueOf(textForTextField.charAt(i)))){
                                JOptionPane.showMessageDialog(null,"You have entered same letter");
                                break;
                            }
                            else {
                                textForTextField.setCharAt(i,jTextField2.getText().charAt(0));
                            }
                        }
                    }
                    jTextField1.setText(String.valueOf(textForTextField).toLowerCase());
                    jTextField2.setText("");

                    if (String.valueOf(jTextField1.getText()).equals(answer)){
                        JOptionPane.showMessageDialog(null,"Congratulations!! You win!");
                        jTextField1.setText("");
                        textForTextField = new StringBuilder();
                        int result = JOptionPane.showConfirmDialog(getContentPane(),"Do you want to Restart?"
                                , "Restart Confirmation ", JOptionPane.YES_NO_CANCEL_OPTION);
                        if (result == JOptionPane.YES_OPTION){

                            jTextField1.setText(String.valueOf(setTextFieldx()));
                            jTextField2.setText("");
                        }
                        else  if (result == JOptionPane.NO_OPTION){
                            jTextField1.setText("");
                            jTextField2.setEditable(false);
                            jButton.setEnabled(false);
                            jButton.removeActionListener(this);
                        }
                        else if (result == JOptionPane.CANCEL_OPTION)
                            System.exit(0);

                    }
                }
            }
        }
        catch (Exception e1){
            JOptionPane.showMessageDialog(null,"Invalid input");
        }

    }
}
